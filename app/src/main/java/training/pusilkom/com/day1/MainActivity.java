package training.pusilkom.com.day1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView text = (TextView) findViewById(R.id.text1);

        final EditText textInput1 = (EditText) findViewById(R.id.textInput1);

        final Intent intent = new Intent(this, HomeActivity.class);

        final Context ctx = getApplicationContext();


        Button button1 = (Button) findViewById(R.id.buttonHalo);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text.setText(textInput1.getText());
                intent.putExtra("name", textInput1.getText().toString());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            }
        });
    }


}
